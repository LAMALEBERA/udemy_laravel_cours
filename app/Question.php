<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Parsedown;
class Question extends Model
{

    protected $fillable = ['title','body'];
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function setTitleAttribute($value){
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }

    public function getUrlAttribute(){
        return route("questions.show", $this->slug);
    }


    public function getCreateDateAttribute(){
        return $this->created_at->diffforHumans();
    }

    public function getStatusAttribute(){
        if($this->best_ansewer_id){

            return "answered-accepted";
        }

        if($this->answers_count>0){
            return "answered";
        }
        return "unanswered";
    }


    public function getBodyHtmlAttribute(){
        return \Parsedown::instance()->text($this->body);
    }

    public function answers(){
      return $this->hasMany(Answer::class);
    }


}
