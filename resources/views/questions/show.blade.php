@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">
                        <div class="card-title">
                            <div class="d-flex align-items-center">
                                <h1>{{$question->title}}</h1>
                                <div class="ml-auto">
                                    <a href="{{route('questions.index')}}" class="btn btn-outline-secondary btn-small">Back to all questions</a>
                                </div>
                            </div>
                            </div>
                            <hr>
                        <div class="media">
                            <div class="d-flex-column vote-controls">
                                <a href="" title="This question is useful" class="vote-up">
                                    <i class="fas fa-caret-up fa-3x"></i>
                                </a>
                                <span class="votes-count">232</span>
                                <a href="" title="This question is not useful" class="vote-down off"> <i class="fas fa-caret-down fa-3x"></i></a>
                                <a href="" title="click to mart  fovarite question (click again to undo)" class="favorite favorited"><i class="fas fa-star fa-3x"></i>
                                </a>
                                <span class="favorites-count"></span>
                            </div>
                            <div class="media-body">
                                {{ $question->body}}
                            <div class="float-right">
                                <span class="text-muted">
                                    Asked by {{ $question->created_date }}
                                </span>
                                <a href="{{$question->user->url}}" class="pr-2">
                                    <img src="{{$question->user->avatar}}" alt="" srcset="">
                                </a>
                                <div class="media-body">
                                    <a href="{{$question->user->url}}">
                                        {{ $question->user->name }}
                                    </a>
                                </div>
                            </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-bady">
                    <div class="card-title">
                        <h2>
                            {{ $question->answers_count. " ". Str::plural('Answer',$question->answers_count)}}
                        </h2>
                        <hr>
                        @foreach ($question->answers as $answer)
                            <div class="media">
                              <div class="d-flex-column vote-controls">
                                        <a href="" title="This question is useful" class="vote-up">
                                            <i class="fas fa-caret-up fa-3x"></i>
                                        </a>
                                        <span class="votes-count">232</span>
                                        <a href="" title="This question is not useful" class="vote-down off"> <i class="fas fa-caret-down fa-3x"></i></a>
                                        <a href="" title="click to mart  fovarite question" class="favorite "><i class="fas fa-check fa-3x"></i>
                                        </a>
                            </div>
                                <div class="media-body">
                                    {{ $answer->body }}
                                    <div class="float-right">
                                        <span class="text-muted">
                                            Answered {{ $answer->created_date }}
                                        </span>
                                        <a href="{{$answer->user->url}}" class="pr-2">
                                            <img src="{{$answer->user->avatar}}" alt="" srcset="">
                                        </a>
                                        <div class="media-body">
                                            <a href="{{$answer->user->url}}">
                                                {{ $answer->user->name }}
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection
